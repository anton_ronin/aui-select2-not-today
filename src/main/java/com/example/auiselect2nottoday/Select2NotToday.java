package com.example.auiselect2nottoday;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import static org.apache.log4j.Logger.getLogger;

public class Select2NotToday extends BaseMacro implements Macro {

    private final Logger logger = getLogger(Select2NotToday.class);

    private static final String TEMPLATE = "templates/select2-not-today.vm";
    private static final String CONFIG_PROPERTIES = "config.properties";

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        try {
            return execute(parameters, body, ((RenderContext) null));
        } catch (MacroException e) {
            throw new MacroExecutionException(e);
        }
    }

    @Override
    public String execute(Map params, String body, RenderContext renderContext) throws MacroException {

        Map<String, Object> context = MacroUtils.defaultVelocityContext();

        User user = AuthenticatedUserThreadLocal.getUser();
        if (user != null) {
            context.put("userFullName", user.getFullName());
            context.put("userEmail", user.getEmail());
        }

        Properties aliases = new Properties();
        try {
            aliases.load(this.getClass().getClassLoader().getResourceAsStream(CONFIG_PROPERTIES));
        } catch (IOException e) {
            logger.fatal("Failed to load system aliases from " + CONFIG_PROPERTIES);
            context.put("isSystemDown", true);
            return VelocityUtils.getRenderedTemplate(TEMPLATE, context);
        }

        // load config
        for (String propName : aliases.stringPropertyNames()) {
            context.put(propName.replace(".", "_"), aliases.getProperty(propName));
        }

        return VelocityUtils.getRenderedTemplate(TEMPLATE, context);
    }

    @Override
    public boolean isInline() {
        return false;
    }

    @Override
    public boolean hasBody() {
        return false;
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

}
