// ---------------------------------------------------------------------------------------------------------------------
// attempt #2
// AUI select2 init didn't work
// ---------------------------------------------------------------------------------------------------------------------
/*
AJS.$(document).ready(function () {

    AJS.$("#select2-example").auiSelect2();

    var options = [
        {id: 'CONF', text: 'Confluence'},
        {id: 'JIRA', text: 'JIRA'},
        {id: 'BAM', text: 'Bamboo'},
        {id: 'JAG', text: 'JIRA Agile'},
        {id: 'CAP', text: 'JIRA Capture'},
        {id: 'AUI', text: 'AUI'}
    ];

    AJS.$("#select2-init-from-array-example").auiSelect2({
        data: options
    });

    AJS.$("#select2-init-via-ajax").auiSelect2({
        placeholder: 'Start typing...',
        minimumInputLength: 2,
        allowClear: true,
        multiple: true,

        ajax: {
            url: "https://api.github.com/search/repositories",
            dataType: 'json',
            delay: 300,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: false
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        templateResult: function (repo) {
            if (repo.loading) return repo.text;
            return repo.full_name || repo.text;

        },
        templateSelection: function (repo) {
            return '<table>' +
                '<tr>' +
                '<td>Repo name:</td><td><b>' + repo.full_name + '</b></td>' +
                '</tr>' +
                '<tr>' +
                '<td>Repo description:</td><td><b>' + repo.description + '></b></td>' +
                '</tr>' +
                '</table>';
        }
    });

});
*/

// ---------------------------------------------------------------------------------------------------------------------
// attempt #3
// AUI select2 init didn't work
// ---------------------------------------------------------------------------------------------------------------------

/*
AJS.$(function () {

    AJS.$("#select2-example").auiSelect2();

    var options = [
        {id: 'CONF', text: 'Confluence'},
        {id: 'JIRA', text: 'JIRA'},
        {id: 'BAM', text: 'Bamboo'},
        {id: 'JAG', text: 'JIRA Agile'},
        {id: 'CAP', text: 'JIRA Capture'},
        {id: 'AUI', text: 'AUI'}
    ];

    AJS.$("#select2-init-from-array-example").auiSelect2({
        data: options
    });

    AJS.$("#select2-init-via-ajax").auiSelect2({
        placeholder: 'Start typing...',
        minimumInputLength: 2,
        allowClear: true,
        multiple: true,

        ajax: {
            url: "https://api.github.com/search/repositories",
            dataType: 'json',
            delay: 300,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: false
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        templateResult: function (repo) {
            if (repo.loading) return repo.text;
            return repo.full_name || repo.text;

        },
        templateSelection: function (repo) {
            return '<table>' +
                '<tr>' +
                '<td>Repo name:</td><td><b>' + repo.full_name + '</b></td>' +
                '</tr>' +
                '<tr>' +
                '<td>Repo description:</td><td><b>' + repo.description + '></b></td>' +
                '</tr>' +
                '</table>';
        }
    });

});
*/

// ---------------------------------------------------------------------------------------------------------------------
// attempt #4
// AUI select2 init didn't work
// ---------------------------------------------------------------------------------------------------------------------

/*
AJS.toInit(function () {

    AJS.$("#select2-example").auiSelect2();

    var options = [
        {id: 'CONF', text: 'Confluence'},
        {id: 'JIRA', text: 'JIRA'},
        {id: 'BAM', text: 'Bamboo'},
        {id: 'JAG', text: 'JIRA Agile'},
        {id: 'CAP', text: 'JIRA Capture'},
        {id: 'AUI', text: 'AUI'}
    ];

    AJS.$("#select2-init-from-array-example").auiSelect2({
        data: options
    });

    AJS.$("#select2-init-via-ajax").auiSelect2({
        placeholder: 'Start typing...',
        minimumInputLength: 2,
        allowClear: true,
        multiple: true,

        ajax: {
            url: "https://api.github.com/search/repositories",
            dataType: 'json',
            delay: 300,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: false
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        templateResult: function (repo) {
            if (repo.loading) return repo.text;
            return repo.full_name || repo.text;

        },
        templateSelection: function (repo) {
            return '<table>' +
                '<tr>' +
                '<td>Repo name:</td><td><b>' + repo.full_name + '</b></td>' +
                '</tr>' +
                '<tr>' +
                '<td>Repo description:</td><td><b>' + repo.description + '></b></td>' +
                '</tr>' +
                '</table>';
        }
    });

});
*/